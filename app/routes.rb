require "#{File.dirname(__FILE__)}/../lib/routing"
require "#{File.dirname(__FILE__)}/../lib/version"

class Routes
  include Routing

  on_message '/start' do |bot, _api, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Hola, #{message.from.first_name}, cómo estás?")
  end

  on_message '/stop' do |bot, _api, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Chau, #{message.from.username}")
  end

  on_message '/time' do |bot, _api, message|
    bot.api.send_message(chat_id: message.chat.id, text: "La hora es, #{Time.now}")
  end

  on_message_pattern %r{/registrar *(?<email>.*)?} do |bot, api, message, args|
    if args['email'].empty?
      bot.api.send_message(chat_id: message.chat.id, text: 'Registración invalida. Ingrese su email')
    else
      api.registrar_usuario(args['email'], message.from.username)
      bot.api.send_message(chat_id: message.chat.id, text: 'Bienvenido')
    end
  end

  on_message '/novedades' do |bot, api, message|
    novedades = api.novedades
    if novedades.empty?
      bot.api.send_message(chat_id: message.chat.id, text: 'No hay novedades')
    else
      respuesta = novedades.map(&:info_basica).join("\n")
      bot.api.send_message(chat_id: message.chat.id, text: respuesta)
    end
  end

  on_message '/tendencias' do |bot, api, message|
    tendencias = api.tendencias
    if tendencias.empty?
      bot.api.send_message(chat_id: message.chat.id, text: 'No hay tendencias')
    else
      respuesta = tendencias.map(&:info_basica).join("\n")
      bot.api.send_message(chat_id: message.chat.id, text: respuesta)
    end
  end

  on_message '/sugerencias' do |bot, api, message|
    sugerencias = api.sugerencias(message.from.username)
    if sugerencias.empty?
      bot.api.send_message(chat_id: message.chat.id, text: 'No hay sugerencias')
    else
      respuesta = sugerencias.map(&:info_basica).join("\n")
      bot.api.send_message(chat_id: message.chat.id, text: respuesta)
    end
  end

  on_message_pattern %r{/detalle *(?<id>.*)?} do |bot, api, message, args|
    if args['id'].empty?
      bot.api.send_message(chat_id: message.chat.id, text: 'No se indico de que contenido se quieren detalles.')
    else
      contenido = api.detalle_contenido(args['id'])
      respuesta = contenido.obtener_detalle
      bot.api.send_message(chat_id: message.chat.id, text: respuesta)
    end
  end

  on_message_pattern %r{/genero *(?<nombre>.*)?} do |bot, api, message, args|
    if args['nombre'].empty?
      bot.api.send_message(chat_id: message.chat.id, text: 'No se indico de que género se quiere contenido.')
    else
      contenidos = api.contenido_por_genero(args['nombre'])
      if contenidos.empty?
        bot.api.send_message(chat_id: message.chat.id, text: 'No hay contenido del género')
      else
        respuesta = contenidos.map(&:info_basica).join("\n")
        bot.api.send_message(chat_id: message.chat.id, text: respuesta)
      end
    end
  end

  on_message_pattern %r{/megusta *(?<id>.*)?} do |bot, api, message, args|
    ids = args['id'].split
    if args['id'].empty?
      bot.api.send_message(chat_id: message.chat.id, text: 'Me gusta invalido. Ingrese el id del contenido')
    elsif ids.count > 3
      bot.api.send_message(chat_id: message.chat.id, text: 'No existe contenido que tenga más de 3 ids.')
    elsif ids.map { |id| id.to_i.to_s != id }.any?
      bot.api.send_message(chat_id: message.chat.id, text: 'Me gusta invalido. Los ids deben ser un números')
    else
      api.registrar_me_gusta(message.from.username, args['id'])
      bot.api.send_message(chat_id: message.chat.id, text: 'Calificación registrada')
    end
  end

  on_message_pattern %r{/guardar *(?<id>.*)?} do |bot, api, message, args|
    if args['id'].empty?
      bot.api.send_message(chat_id: message.chat.id, text: 'No se indicó el contenido a guardar.')
    elsif args['id'].to_i.to_s != args['id']
      bot.api.send_message(chat_id: message.chat.id, text: 'Contenido invalido. El id debe ser un número')
    else
      api.agregar_a_lista(message.from.username, args['id'].to_i)
      bot.api.send_message(chat_id: message.chat.id, text: 'Se agregó a tu lista correctamente')
    end
  end

  on_message '/milista' do |bot, api, message|
    favoritos = api.consultar_favoritos(message.from.username)
    if favoritos.empty?
      bot.api.send_message(chat_id: message.chat.id, text: 'No tenes favoritos.')
    else
      respuesta = favoritos.map do |contenido|
        "#{contenido.titulo} (#{contenido.id}), #{contenido.genero}"
      end.join("\n")
      bot.api.send_message(chat_id: message.chat.id, text: respuesta)
    end
  end

  on_message '/version' do |bot, _api, message|
    bot.api.send_message(chat_id: message.chat.id, text: Version.current)
  end

  default do |bot, _api, message|
    bot.api.send_message(chat_id: message.chat.id, text: 'Uh? No te entiendo! Me repetis la pregunta?')
  end
end
