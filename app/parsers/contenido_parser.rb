require_relative 'mini_serie_parser'
require_relative 'pelicula_parser'
require_relative 'serie_parser'

class ContenidoParser
  def crear(contenido)
    {
      'Pelicula' => ->(pelicula) { PeliculaParser.crear(pelicula) },
      'MiniSerie' => ->(mini_serie) { MiniSerieParser.crear(mini_serie) },
      'Serie' => ->(serie) { SerieParser.crear(serie) }
    }[contenido['tipo']].call(contenido)
  end
end
