require_relative '../api/mini_serie'
require_relative './capitulo_parser'

class MiniSerieParser
  def self.crear(mini_serie)
    capitulos = mini_serie['capitulos'].map { |capitulo| CapituloParser.crear(capitulo) }
    MiniSerie.new(
      id: mini_serie['id'],
      titulo: mini_serie['titulo'],
      anio_de_estreno: mini_serie['anio_de_estreno'],
      pais_de_origen: mini_serie['pais_de_origen'],
      genero: mini_serie['genero'],
      audiencia: mini_serie['audiencia'],
      actores: mini_serie['actores'],
      fecha_de_alta: Date.parse(mini_serie['fecha_de_alta']),
      capitulos: capitulos
    )
  end
end
