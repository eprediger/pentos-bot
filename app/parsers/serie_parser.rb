require_relative '../api/serie'
require_relative './temporada_parser'

class SerieParser
  def self.crear(serie)
    temporadas = serie['temporadas'].map { |temporada| TemporadaParser.crear(temporada) }
    Serie.new(
      id: serie['id'],
      titulo: serie['titulo'],
      anio_de_estreno: serie['anio_de_estreno'],
      pais_de_origen: serie['pais_de_origen'],
      genero: serie['genero'],
      audiencia: serie['audiencia'],
      actores: serie['actores'],
      fecha_de_alta: Date.parse(serie['fecha_de_alta']),
      temporadas: temporadas
    )
  end
end
