require_relative '../api/pelicula'

class PeliculaParser
  def self.crear(pelicula)
    Pelicula.new(
      id: pelicula['id'],
      titulo: pelicula['titulo'],
      director: pelicula['director'],
      anio_de_estreno: pelicula['anio_de_estreno'],
      pais_de_origen: pelicula['pais_de_origen'],
      genero: pelicula['genero'],
      audiencia: pelicula['audiencia'],
      actores: pelicula['actores'],
      duracion: pelicula['duracion'],
      fecha_de_alta: Date.parse(pelicula['fecha_de_alta'])
    )
  end
end
