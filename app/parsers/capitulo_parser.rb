require_relative '../api/capitulo'

class CapituloParser
  def self.crear(capitulo)
    Capitulo.new(
      numero: capitulo['numero'],
      titulo: capitulo['titulo'],
      director: capitulo['director'],
      duracion: capitulo['duracion']
    )
  end
end
