require_relative '../api/temporada'
require_relative './capitulo_parser'

class TemporadaParser
  def self.crear(temporada)
    Temporada.new(
      numero: temporada['numero'],
      capitulos: temporada['capitulos'].map { |capitulo| CapituloParser.crear(capitulo) }
    )
  end
end
