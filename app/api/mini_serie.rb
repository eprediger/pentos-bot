require_relative 'contenido'

class MiniSerie < Contenido
  attr_reader :capitulos

  def initialize(id:, titulo:, anio_de_estreno:, pais_de_origen:,
                 genero:, audiencia:, actores:, fecha_de_alta:, capitulos:)
    super(tipo: 'Mini serie',
          id: id,
          titulo: titulo,
          actores: actores,
          anio_de_estreno: anio_de_estreno,
          audiencia: audiencia,
          genero: genero,
          pais_de_origen: pais_de_origen,
          fecha_de_alta: fecha_de_alta
    )

    @capitulos = capitulos
  end

  def obtener_detalle
    <<~DETALLE.chomp
      #{detalle_contenido}
      Capítulos:
      #{@capitulos.map do |capitulo|
        "  #{capitulo.obtener_detalle}"
      end.join("\n")}
    DETALLE
  end
end
