require_relative 'contenido'

class Pelicula < Contenido
  attr_reader :id, :titulo, :director, :anio_de_estreno, :pais_de_origen,
              :genero, :audiencia, :actores, :duracion, :fecha_de_alta

  def initialize(id:, titulo:, director:, anio_de_estreno:, pais_de_origen:,
                 genero:, audiencia:, actores:, duracion:, fecha_de_alta:)
    super(tipo: 'Peli',
          id: id,
          titulo: titulo,
          anio_de_estreno: anio_de_estreno,
          pais_de_origen: pais_de_origen,
          genero: genero,
          audiencia: audiencia,
          actores: actores,
          fecha_de_alta: fecha_de_alta)
    @director = director
    @duracion = duracion
  end

  def obtener_detalle
    <<~DETALLE.chomp
      #{@tipo}: #{@titulo} (#{@id})
      Actores: #{@actores}
      Audiencia: #{@audiencia}
      Año de estreno: #{@anio_de_estreno}
      Director: #{@director}
      Duración: #{@duracion} minutos
      Género: #{@genero}
      País de origen: #{@pais_de_origen}
      Fecha de alta: #{@fecha_de_alta}
    DETALLE
  end
end
