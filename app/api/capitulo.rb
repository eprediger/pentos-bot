class Capitulo
  attr_reader :numero, :titulo, :director, :duracion

  def initialize(numero:, titulo:, director:, duracion:)
    @numero = numero
    @titulo = titulo
    @director = director
    @duracion = duracion
  end

  def obtener_detalle
    "#{numero}: \"#{@titulo}\" (#{@duracion} minutos). Director: #{@director}"
  end
end
