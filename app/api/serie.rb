require_relative 'contenido'

class Serie < Contenido
  attr_reader :temporadas

  def initialize(id:, titulo:, anio_de_estreno:, pais_de_origen:,
                 genero:, audiencia:, actores:, fecha_de_alta:, temporadas:)
    super(tipo: 'Serie',
          id: id,
          titulo: titulo,
          actores: actores,
          anio_de_estreno: anio_de_estreno,
          audiencia: audiencia,
          genero: genero,
          pais_de_origen: pais_de_origen,
          fecha_de_alta: fecha_de_alta
    )

    @temporadas = temporadas
  end

  def obtener_detalle
    <<~DETALLE.chomp
      #{detalle_contenido}
      #{@temporadas.map do |temporada|
          temporada.obtener_detalle.to_s
        end.join("\n")}
    DETALLE
  end
end
