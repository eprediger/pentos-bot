class Temporada
  attr_reader :capitulos, :numero

  def initialize(numero:, capitulos:)
    @numero = numero
    @capitulos = capitulos
  end

  def obtener_detalle
    <<~DETALLE.chomp
      Temporada #{@numero}
        Capítulos:
      #{@capitulos.map do |capitulo|
          "    #{capitulo.obtener_detalle}"
        end.join("\n")}
    DETALLE
  end
end
