class Contenido
  attr_reader :id, :titulo, :anio_de_estreno, :pais_de_origen,
              :genero, :audiencia, :actores, :fecha_de_alta, :tipo

  def initialize(id:, titulo:, anio_de_estreno:, pais_de_origen:,
                 genero:, audiencia:, actores:, fecha_de_alta:, tipo:)
    @tipo = tipo
    @id = id
    @titulo = titulo
    @actores = actores
    @anio_de_estreno = anio_de_estreno
    @audiencia = audiencia
    @genero = genero
    @pais_de_origen = pais_de_origen
    @fecha_de_alta = fecha_de_alta
  end

  def info_basica
    "#{@tipo}: #{@titulo} (#{@id})"
  end

  def detalle_contenido
    <<~DETALLE.chomp
      #{@tipo}: #{@titulo} (#{@id})
      Actores: #{@actores}
      Audiencia: #{@audiencia}
      Año de estreno: #{@anio_de_estreno}
      Género: #{@genero}
      País de origen: #{@pais_de_origen}
      Fecha de alta: #{@fecha_de_alta}
    DETALLE
  end
end
