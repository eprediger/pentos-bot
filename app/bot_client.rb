require 'telegram/bot'
require_relative 'api'
require "#{File.dirname(__FILE__)}/../app/routes"

class BotClient
  def initialize(token = ENV['TELEGRAM_TOKEN'], api = Api.new, log_level = ENV['LOG_LEVEL'])
    @token = token
    @logger = Logger.new($stdout)
    @logger.level = log_level.to_i
    @api = api
  end

  def start
    @logger.info "Starting bot version:#{Version.current}"
    @logger.info "token is #{@token}"
    run_client do |bot|
      bot.listen { |message| handle_message(message, bot, @api) }
    rescue StandardError => e
      @logger.fatal e.message
    end
  end

  def run_once
    run_client do |bot|
      bot.fetch_updates { |message| handle_message(message, bot, @api) }
    end
  end

  private

  def run_client(&block)
    Telegram::Bot::Client.run(@token, logger: @logger) { |bot| block.call bot }
  end

  def handle_message(message, bot, api)
    @logger.debug "From: @#{message.from.username}, message: #{message.inspect}"
    begin
      Routes.new.handle(bot, api, message)
    rescue RequestInvalidoError => e
      @logger.info("Request invalido: #{e.message}")
      bot.api.send_message(chat_id: message.chat.id, text: e.message)
    rescue StandardError => e
      @logger.info("Error inesperado: #{e.message}")
      bot.api.send_message(chat_id: message.chat.id, text: 'Error inesperado. Intente nuevamente en unos minutos')
    end
  end
end
