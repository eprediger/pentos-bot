require_relative 'errors/request_invalido_error'
require_relative 'errors/conexion_error'
require_relative 'api/pelicula'
require_relative 'parsers/contenido_parser'

class Api
  def initialize(url = ENV['API_URL'], log_level = ENV['LOG_LEVEL'], key = ENV['BOT_API_KEY'])
    @url = url
    @logger = Logger.new($stdout)
    @logger.level = log_level.to_i
    @key = key
  end

  def registrar_usuario(email, nombre)
    @logger.info "Registrando usuario con email #{email} y nombre #{nombre}"

    response = Faraday.post("#{@url}/usuarios", {
      email: email,
      nombre: nombre
    }.to_json, headers)
    handle_errors(response)
  end

  def novedades
    @logger.info 'Buscando novedades'

    response = Faraday.get("#{@url}/novedades", {}, headers)
    handle_errors(response)

    body = JSON.parse(response.body)
    body.map { |contenido| crear_contenido contenido }
  end

  def sugerencias(usuario)
    @logger.info 'Buscando sugerencias'

    response = Faraday.get("#{@url}/usuarios/#{usuario}/sugerencias", {}, headers)
    handle_errors(response)

    body = JSON.parse(response.body)
    body.map { |contenido| crear_contenido contenido }
  end

  def detalle_contenido(id)
    @logger.info "Buscando detalle de contenido con id #{id}"

    response = Faraday.get("#{@url}/contenidos/#{id}", {}, headers)
    handle_errors(response)

    body = JSON.parse(response.body)
    crear_contenido(body)
  end

  def contenido_por_genero(nombre)
    @logger.info "Buscando contenidos del género #{nombre}"

    response = Faraday.get("#{@url}/contenidos", { genero: nombre }, headers)
    handle_errors(response)

    body = JSON.parse(response.body)
    body.map { |contenido| crear_contenido contenido }
  end

  def registrar_me_gusta(usuario, id_contenido)
    @logger.info "Registrando me gusta de usuario #{usuario} en contenido #{id_contenido}"
    ids = id_contenido.split
    if ids.count == 1
      response = Faraday.patch("#{@url}/usuarios/#{usuario}/contenido/#{id_contenido}", {
        me_gusta: true
      }.to_json, headers)
    elsif ids.count == 2
      response = Faraday.patch("#{@url}/usuarios/#{usuario}/contenido/#{ids[0]}/#{ids[1]}", {
        me_gusta: true
      }.to_json, headers)
    elsif ids.count == 3
      response = Faraday.patch("#{@url}/usuarios/#{usuario}/contenido/#{ids[0]}/#{ids[1]}/#{ids[2]}", {
        me_gusta: true
      }.to_json, headers)
    end

    handle_errors(response)
  end

  def agregar_a_lista(usuario, id_contenido)
    @logger.info "Agregando el contenido #{id_contenido} a la lista de favoritos de #{usuario}"
    response = Faraday.post("#{@url}/usuarios/#{usuario}/favoritos", {
      id_contenido: id_contenido
    }.to_json, headers)
    handle_errors(response)
  end

  def consultar_favoritos(usuario)
    @logger.info "Consultando la lista de favoritos de #{usuario}"
    response = Faraday.get("#{@url}/usuarios/#{usuario}/favoritos", {}, headers)
    handle_errors(response)

    body = JSON.parse(response.body)
    body.map { |contenido| crear_contenido contenido }
  end

  def tendencias
    @logger.info 'Buscando tendencias'

    response = Faraday.get("#{@url}/tendencias", {}, headers)
    handle_errors(response)

    body = JSON.parse(response.body)
    body.map { |contenido| crear_contenido contenido }
  end

  private

  def handle_errors(response)
    body = JSON.parse(response.body)

    if response.status.between?(400, 499)
      @logger.info "RequestInvalidoError: #{body.to_json}"
      raise RequestInvalidoError, body['mensaje']
    elsif response.status.between?(500, 599)
      @logger.error "ConexionError: #{body.to_json}"
      raise ConexionError
    end
  end

  def crear_contenido(contenido)
    ContenidoParser.new.crear(contenido)
  end

  def headers
    {
      'Content-Type' => 'application/json',
      'Authorization' => @key,
      'Accept' => 'application/json'
    }
  end
end
