require 'spec_helper'

describe 'Serie' do
  it 'debe devolver el detalle' do
    capitulos = [CapituloEjemplo.crear.con_numero(1).con_titulo('Efectuar lo acordado').con_duracion(45).con_director('Jesús Colmenar').build]

    temporadas = [TemporadaEjemplo.crear.con_numero(1).con_capitulos(capitulos).build,
                  TemporadaEjemplo.crear.con_numero(2).con_capitulos(capitulos).build]

    serie = SerieEjemplo.crear
                        .con_id(2)
                        .con_titulo('La casa de papel')
                        .con_actores('Úrsula Corberó, Álvaro Morte, Itzar Ituño')
                        .con_anio_estreno(2017)
                        .con_audiencia('16+')
                        .con_genero('Suspenso')
                        .con_pais_origen('España')
                        .con_fecha_de_alta('2020-10-01')
                        .con_temporadas(temporadas)
                        .build

    detalle_esperado = <<~DETALLE.chomp
      Serie: La casa de papel (2)
      Actores: Úrsula Corberó, Álvaro Morte, Itzar Ituño
      Audiencia: 16+
      Año de estreno: 2017
      Género: Suspenso
      País de origen: España
      Fecha de alta: 2020-10-01
      Temporada 1
        Capítulos:
          1: "Efectuar lo acordado" (45 minutos). Director: Jesús Colmenar
      Temporada 2
        Capítulos:
          1: "Efectuar lo acordado" (45 minutos). Director: Jesús Colmenar
    DETALLE

    expect(serie.obtener_detalle).to eq(detalle_esperado)
  end
end
