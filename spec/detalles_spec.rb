require 'spec_helper'
require_relative 'bot_client_ejemplo'
require_relative '../app/errors/request_invalido_error'
require_relative '../app/errors/conexion_error'
require_relative '../app/api/pelicula'
require_relative './ejemplos/mini_serie_ejemplo'
require_relative './ejemplos/serie_ejemplo'

require "#{File.dirname(__FILE__)}/../app/bot_client"

describe 'Detalles' do
  let(:token) { 'fake_token' }
  let(:api_mock) { instance_double('Api') }
  let(:app) do
    BotClientEjemplo.crear
                    .con_token(token)
                    .con_api(api_mock)
                    .build
  end

  it 'devuelve la información asociada a la película indicada' do
    stub_get_updates(token, '/detalle 1')

    detalle = PeliculaEjemplo.crear
                             .con_id(1)
                             .con_titulo('Star Wars')
                             .con_director('George Lucas')
                             .build

    stub_send_message(token, <<~PELIS.chomp
      Peli: Star Wars (1)
      Actores: Mark Hamill, Harrison Ford, Carrie Fisher
      Audiencia: 10+
      Año de estreno: 1977
      Director: George Lucas
      Duración: 121 minutos
      Género: Ciencia Ficción
      País de origen: Estados Unidos
      Fecha de alta: 2020-10-01
    PELIS
    )

    allow(api_mock).to receive(:detalle_contenido).with('1').and_return(detalle)
    app.run_once
  end

  it 'devuelve "No se indico de que contenido se quieren detalles." cuando no se ingresa un id' do
    stub_get_updates(token, '/detalle')

    mensaje = 'No se indico de que contenido se quieren detalles.'
    stub_send_message(token, mensaje)

    expect(api_mock).not_to receive(:detalle_contenido)
    app.run_once
  end

  it 'devuelve "No encontré resultados" cuando se consulta por un id inexistente' do
    stub_get_updates(token, '/detalle 5')

    mensaje = 'No se encontraron resultados para el id 5'

    stub_send_message(token, mensaje)
    allow(api_mock).to receive(:detalle_contenido).with('5').and_raise(RequestInvalidoError.new(mensaje))
    app.run_once
  end

  it 'devuelve la información asociada a la mini serie indicada' do
    stub_get_updates(token, '/detalle 1')

    capitulos = [
      CapituloEjemplo.crear.con_numero(1).con_titulo('1:23:45').con_duracion(58).con_director('Johan Renck').build,
      CapituloEjemplo.crear.con_numero(2).con_titulo('Please Remain Calm').con_duracion(65).con_director('Johan Renck').build,
      CapituloEjemplo.crear.con_numero(3).con_titulo('Open Wide, O Earth').con_duracion(65).con_director('Johan Renck').build,
      CapituloEjemplo.crear.con_numero(4).con_titulo('The Happiness of All Mankind').con_duracion(67).con_director('Johan Renck').build,
      CapituloEjemplo.crear.con_numero(5).con_titulo('Vichnaya Pamyat').con_duracion(72).con_director('Johan Renck').build
    ]

    detalle = MiniSerieEjemplo.crear
                              .con_id(1)
                              .con_titulo('Chernobyl')
                              .con_actores('Jared Harris, Stellan Skarsgård, Emily Watson')
                              .con_anio_estreno(2019)
                              .con_audiencia('16+')
                              .con_genero('Drama Histórico')
                              .con_pais_origen('Estados Unidos')
                              .con_fecha_de_alta('2020-03-13')
                              .con_capitulos(capitulos)
                              .build

    stub_send_message(token, <<~MINISERIE.chomp
      Mini serie: Chernobyl (1)
      Actores: Jared Harris, Stellan Skarsgård, Emily Watson
      Audiencia: 16+
      Año de estreno: 2019
      Género: Drama Histórico
      País de origen: Estados Unidos
      Fecha de alta: 2020-03-13
      Capítulos:
        1: "1:23:45" (58 minutos). Director: Johan Renck
        2: "Please Remain Calm" (65 minutos). Director: Johan Renck
        3: "Open Wide, O Earth" (65 minutos). Director: Johan Renck
        4: "The Happiness of All Mankind" (67 minutos). Director: Johan Renck
        5: "Vichnaya Pamyat" (72 minutos). Director: Johan Renck
    MINISERIE
    )

    allow(api_mock).to receive(:detalle_contenido).with('1').and_return(detalle)
    app.run_once
  end

  it 'devuelve la información asociada a la serie indicada' do
    stub_get_updates(token, '/detalle 1')

    capitulos = [CapituloEjemplo.crear.con_numero(1).con_titulo('Efectuar lo acordado').con_duracion(45).con_director('Jesús Colmenar').build]

    temporadas = [TemporadaEjemplo.crear.con_numero(1).con_capitulos(capitulos).build,
                  TemporadaEjemplo.crear.con_numero(2).con_capitulos(capitulos).build]

    detalle = SerieEjemplo.crear
                          .con_id(1)
                          .con_titulo('La casa de papel')
                          .con_actores('Úrsula Corberó, Álvaro Morte, Itzar Ituño')
                          .con_anio_estreno(2017)
                          .con_audiencia('16+')
                          .con_genero('Suspenso')
                          .con_pais_origen('España')
                          .con_fecha_de_alta('2020-10-01')
                          .con_temporadas(temporadas)
                          .build

    stub_send_message(token, <<~SERIE.chomp
      Serie: La casa de papel (1)
      Actores: Úrsula Corberó, Álvaro Morte, Itzar Ituño
      Audiencia: 16+
      Año de estreno: 2017
      Género: Suspenso
      País de origen: España
      Fecha de alta: 2020-10-01
      Temporada 1
        Capítulos:
          1: "Efectuar lo acordado" (45 minutos). Director: Jesús Colmenar
      Temporada 2
        Capítulos:
          1: "Efectuar lo acordado" (45 minutos). Director: Jesús Colmenar
    SERIE
    )

    allow(api_mock).to receive(:detalle_contenido).with('1').and_return(detalle)
    app.run_once
  end
end
