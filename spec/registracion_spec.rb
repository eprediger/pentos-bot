require 'spec_helper'
require_relative 'bot_client_ejemplo'
require_relative '../app/errors/request_invalido_error'
require_relative '../app/errors/conexion_error'

describe 'Registro' do
  let(:token) { 'fake_token' }
  let(:api_mock) { instance_double('Api') }
  let(:app) do
    BotClientEjemplo.crear
                    .con_token(token)
                    .con_api(api_mock)
                    .build
  end

  it 'devuelve "Bienvenido" cuando se envía /registrar <email>' do
    stub_get_updates(token, '/registrar usuario@test.com')
    stub_send_message(token, 'Bienvenido')

    expect(api_mock).to receive(:registrar_usuario).with('usuario@test.com', 'egutter')

    app.run_once
  end

  it 'devuelve "Bienvenido" cuando se envía /registrar <email> con varios espacios' do
    stub_get_updates(token, '/registrar     usuario@test.com')
    stub_send_message(token, 'Bienvenido')

    expect(api_mock).to receive(:registrar_usuario).with('usuario@test.com', 'egutter')

    app.run_once
  end

  it 'devuelve "Registración invalida. Ingrese su email" cuando se envía /registrar sin email' do
    stub_get_updates(token, '/registrar')
    stub_send_message(token, 'Registración invalida. Ingrese su email')

    expect(api_mock).not_to receive(:registrar_usuario)

    app.run_once
  end

  it 'devuelve el mensaje recibido si ocurre una excepción en el request' do
    stub_get_updates(token, '/registrar unemailnovalido')
    stub_send_message(token, 'Email inválido')

    allow(api_mock).to receive(:registrar_usuario).and_raise(RequestInvalidoError.new('Email inválido'))

    app.run_once
  end

  it 'devuelve "Error inesperado. Intente nuevamente en unos minutos" si ocurre una excepción inesperada' do
    stub_get_updates(token, '/registrar usuario@test.com')
    stub_send_message(token, 'Error inesperado. Intente nuevamente en unos minutos')

    allow(api_mock).to receive(:registrar_usuario).and_raise(ConexionError.new)

    app.run_once
  end
end
