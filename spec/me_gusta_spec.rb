require 'spec_helper'
require_relative 'bot_client_ejemplo'
require_relative '../app/errors/request_invalido_error'
require_relative '../app/errors/conexion_error'

describe 'Me gusta' do
  let(:token) { 'fake_token' }
  let(:api_mock) { instance_double('Api') }
  let(:app) do
    BotClientEjemplo.crear
                    .con_token(token)
                    .con_api(api_mock)
                    .build
  end

  it 'devuelve "Calificación registrada" si se registra correctamente' do
    stub_get_updates(token, '/megusta 1', 'egutter')
    stub_send_message(token, 'Calificación registrada')
    allow(api_mock).to receive(:registrar_me_gusta)

    app.run_once

    expect(api_mock).to have_received(:registrar_me_gusta).with('egutter', '1')
  end

  it 'devuelve "Me gusta invalido. Ingrese el id del contenido" cuando se envía /megusta sin el id del contenido' do
    stub_get_updates(token, '/megusta')
    stub_send_message(token, 'Me gusta invalido. Ingrese el id del contenido')

    expect(api_mock).not_to receive(:registrar_me_gusta)

    app.run_once
  end

  it 'devuelve "Me gusta invalido. El id debe ser un número" cuando se envía /megusta con un id que no es numérico' do
    stub_get_updates(token, '/megusta una letra')
    stub_send_message(token, 'Me gusta invalido. Los ids deben ser un números')

    expect(api_mock).not_to receive(:registrar_me_gusta)

    app.run_once
  end

  it 'devuelve "Calificación no registrada, no viste este contenido" si no se vió el contenido' do
    stub_get_updates(token, '/megusta 1')
    stub_send_message(token, 'Calificación no registrada, no viste este contenido')

    allow(api_mock).to receive(:registrar_me_gusta).and_raise(RequestInvalidoError.new('Calificación no registrada, no viste este contenido'))

    app.run_once
  end

  it 'devuelve "Contenido no existente" si no existe el contenido' do
    stub_get_updates(token, '/megusta 1')
    stub_send_message(token, 'Contenido no existente')

    allow(api_mock).to receive(:registrar_me_gusta).and_raise(RequestInvalidoError.new('Contenido no existente'))

    app.run_once
  end

  it 'devuelve "Calificación registrada" si se intenta likear un capitulo de una mini serie' do
    stub_get_updates(token, '/megusta 1 1', 'egutter')
    stub_send_message(token, 'Calificación registrada')
    allow(api_mock).to receive(:registrar_me_gusta)

    app.run_once

    expect(api_mock).to have_received(:registrar_me_gusta).with('egutter', '1 1')
  end

  it 'devuelve "Calificación registrada" si se intenta likear un capitulo de una serie' do
    stub_get_updates(token, '/megusta 1 1 2', 'egutter')
    stub_send_message(token, 'Calificación registrada')
    allow(api_mock).to receive(:registrar_me_gusta)

    app.run_once

    expect(api_mock).to have_received(:registrar_me_gusta).with('egutter', '1 1 2')
  end

  it 'devuelve "Me gusta invalido. Ingrese el id del contenido" si se intenta likear un contenido con mas de 3 ids' do
    stub_get_updates(token, '/megusta 1 1 2 3', 'egutter')
    stub_send_message(token, 'No existe contenido que tenga más de 3 ids.')
    allow(api_mock).to receive(:registrar_me_gusta)

    app.run_once

    expect(api_mock).not_to receive(:registrar_me_gusta)
  end
end
