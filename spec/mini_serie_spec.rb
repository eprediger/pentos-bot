require 'spec_helper'

describe 'Mini Serie' do
  it 'debe devolver el detalle' do
    capitulos = [
      CapituloEjemplo.crear.con_numero(1).con_titulo('1:23:45').con_duracion(58).con_director('Johan Renck').build,
      CapituloEjemplo.crear.con_numero(2).con_titulo('Please Remain Calm').con_duracion(65).con_director('Johan Renck').build,
      CapituloEjemplo.crear.con_numero(3).con_titulo('Open Wide, O Earth').con_duracion(65).con_director('Johan Renck').build,
      CapituloEjemplo.crear.con_numero(4).con_titulo('The Happiness of All Mankind').con_duracion(67).con_director('Johan Renck').build,
      CapituloEjemplo.crear.con_numero(5).con_titulo('Vichnaya Pamyat').con_duracion(72).con_director('Johan Renck').build
    ]

    mini_serie = MiniSerieEjemplo.crear
                                 .con_id(3)
                                 .con_titulo('Chernobyl')
                                 .con_actores('Jared Harris, Stellan Skarsgård, Emily Watson')
                                 .con_anio_estreno(2019)
                                 .con_audiencia('16+')
                                 .con_genero('Drama Histórico')
                                 .con_pais_origen('Estados Unidos')
                                 .con_fecha_de_alta('2020-03-13')
                                 .con_capitulos(capitulos)
                                 .build

    detalle_esperado = <<~DETALLE.chomp
      Mini serie: Chernobyl (3)
      Actores: Jared Harris, Stellan Skarsgård, Emily Watson
      Audiencia: 16+
      Año de estreno: 2019
      Género: Drama Histórico
      País de origen: Estados Unidos
      Fecha de alta: 2020-03-13
      Capítulos:
        1: "1:23:45" (58 minutos). Director: Johan Renck
        2: "Please Remain Calm" (65 minutos). Director: Johan Renck
        3: "Open Wide, O Earth" (65 minutos). Director: Johan Renck
        4: "The Happiness of All Mankind" (67 minutos). Director: Johan Renck
        5: "Vichnaya Pamyat" (72 minutos). Director: Johan Renck
    DETALLE

    expect(mini_serie.obtener_detalle).to eq(detalle_esperado)
  end
end
