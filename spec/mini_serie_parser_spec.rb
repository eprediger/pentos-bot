require 'spec_helper'
require_relative '../app/parsers/mini_serie_parser'
require_relative './ejemplos/mini_serie_ejemplo'

describe 'MiniSerieParser' do
  it 'debe crear la mini serie con sus atributos' do
    mini_serie_esperada = MiniSerieEjemplo.crear
                                          .con_id(1)
                                          .con_titulo('titulo')
                                          .con_actores('actores')
                                          .con_anio_estreno(2021)
                                          .con_audiencia('audiencia')
                                          .con_genero('genero')
                                          .con_pais_origen('pais')
                                          .build

    datos_mini_serie = {
      'id' => 1,
      'titulo' => 'titulo',
      'actores' => 'actores',
      'anio_de_estreno' => 2021,
      'audiencia' => 'audiencia',
      'genero' => 'genero',
      'pais_de_origen' => 'pais',
      'fecha_de_alta' => '2020-03-13',
      'capitulos' => []
    }

    mini_serie_creada = MiniSerieParser.crear(datos_mini_serie)

    expect(mini_serie_creada.id).to eq(mini_serie_esperada.id)
    expect(mini_serie_creada.titulo).to eq(mini_serie_esperada.titulo)
    expect(mini_serie_creada.genero).to eq(mini_serie_esperada.genero)
    expect(mini_serie_creada.actores).to eq(mini_serie_esperada.actores)
    expect(mini_serie_creada.anio_de_estreno).to eq(mini_serie_esperada.anio_de_estreno)
    expect(mini_serie_creada.audiencia).to eq(mini_serie_esperada.audiencia)
    expect(mini_serie_creada.pais_de_origen).to eq(mini_serie_esperada.pais_de_origen)
  end
end
