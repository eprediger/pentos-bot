require 'spec_helper'

describe 'Pelicula' do
  it 'debe devolver el detalle' do
    pelicula = PeliculaEjemplo.crear
                              .con_id(1)
                              .con_titulo('Star Wars')
                              .con_actores('Mark Hamill, Harrison Ford, Carrie Fisher')
                              .con_audiencia('10+')
                              .con_anio_estreno(1977)
                              .con_director('George Lucas')
                              .con_duracion(121)
                              .con_genero('Ciencia Ficción')
                              .con_pais_origen('Estados Unidos')
                              .con_fecha_de_alta(Date.parse('2020-10-01'))
                              .build

    detalle_esperado = <<~DETALLE.chomp
      Peli: Star Wars (1)
      Actores: Mark Hamill, Harrison Ford, Carrie Fisher
      Audiencia: 10+
      Año de estreno: 1977
      Director: George Lucas
      Duración: 121 minutos
      Género: Ciencia Ficción
      País de origen: Estados Unidos
      Fecha de alta: 2020-10-01
    DETALLE

    expect(pelicula.obtener_detalle).to eq(detalle_esperado)
  end
end
