require 'spec_helper'

describe 'Temporada' do
  it 'debe devolver el detalle' do
    capitulos = [CapituloEjemplo.crear.con_numero(1).con_titulo('1:23:45').con_duracion(58).con_director('Johan Renck').build]

    temporada = TemporadaEjemplo.crear
                                .con_numero(1)
                                .con_capitulos(capitulos)
                                .build

    detalle_esperado = <<~DETALLE.chomp
      Temporada 1
        Capítulos:
          1: "1:23:45" (58 minutos). Director: Johan Renck
    DETALLE

    expect(temporada.obtener_detalle).to eq(detalle_esperado)
  end
end
