require 'spec_helper'
require_relative 'bot_client_ejemplo'
require_relative '../app/errors/request_invalido_error'
require_relative '../app/errors/conexion_error'
require_relative '../app/api/pelicula'

require "#{File.dirname(__FILE__)}/../app/bot_client"

describe 'Novedades' do
  let(:token) { 'fake_token' }
  let(:api_mock) { instance_double('Api') }
  let(:app) do
    BotClientEjemplo.crear
                    .con_token(token)
                    .con_api(api_mock)
                    .build
  end

  it 'devuelve las últimas 3 películas guardadas' do
    stub_get_updates(token, '/novedades')
    novedades = [
      PeliculaEjemplo.crear
                     .con_id(2)
                     .con_titulo('Esperando a la Carroza')
                     .con_director('Alejandro Doria')
                     .build,
      PeliculaEjemplo.crear
                     .con_id(3)
                     .con_titulo('James Bond')
                     .con_director('Sam Mendes')
                     .build,
      PeliculaEjemplo.crear
                     .con_id(4)
                     .con_titulo('Harry Potter')
                     .con_director('David Yates')
                     .build
    ]

    stub_send_message(token, <<~PELIS.chomp
      Peli: Esperando a la Carroza (2)
      Peli: James Bond (3)
      Peli: Harry Potter (4)
    PELIS
    )

    allow(api_mock).to receive(:novedades).and_return(novedades)
    app.run_once
  end

  it 'devuelve "No hay novedades" si no hay novedades' do
    stub_get_updates(token, '/novedades')
    stub_send_message(token, 'No hay novedades')

    allow(api_mock).to receive(:novedades).and_return([])
    app.run_once
  end

  it 'devuelve "Error inesperado. Intente nuevamente en unos minutos" si ocurre una excepción inesperada' do
    stub_get_updates(token, '/novedades')
    stub_send_message(token, 'Error inesperado. Intente nuevamente en unos minutos')

    allow(api_mock).to receive(:novedades).and_raise(ConexionError.new)

    app.run_once
  end

  it 'devuelve mini series y peliculas' do
    stub_get_updates(token, '/novedades')
    novedades = [
      PeliculaEjemplo.crear
                     .con_id(2)
                     .con_titulo('Esperando a la Carroza')
                     .con_director('Alejandro Doria')
                     .build,
      MiniSerieEjemplo.crear
                      .con_id(3)
                      .con_titulo('Chernobyl')
                      .build
    ]

    stub_send_message(token, <<~NOVEDADES.chomp
      Peli: Esperando a la Carroza (2)
      Mini serie: Chernobyl (3)
    NOVEDADES
    )

    allow(api_mock).to receive(:novedades).and_return(novedades)
    app.run_once
  end

  it 'devuelve mini series, peliculas y series' do
    stub_get_updates(token, '/novedades')
    novedades = [
      PeliculaEjemplo.crear
                     .con_id(2)
                     .con_titulo('Esperando a la Carroza')
                     .con_director('Alejandro Doria')
                     .build,
      MiniSerieEjemplo.crear
                      .con_id(3)
                      .con_titulo('Chernobyl')
                      .build,
      SerieEjemplo.crear
                  .con_id(4)
                  .con_titulo('La casa de papel')
                  .build
    ]

    stub_send_message(token, <<~NOVEDADES.chomp
      Peli: Esperando a la Carroza (2)
      Mini serie: Chernobyl (3)
      Serie: La casa de papel (4)
    NOVEDADES
    )

    allow(api_mock).to receive(:novedades).and_return(novedades)
    app.run_once
  end
end
