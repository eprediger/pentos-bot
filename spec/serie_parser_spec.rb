require 'spec_helper'
require_relative '../app/parsers/serie_parser'
require_relative './ejemplos/serie_ejemplo'

describe 'SerieParser' do
  it 'debe crear la serie con sus atributos' do
    serie_esperada = SerieEjemplo.crear
                                 .con_id(1)
                                 .con_titulo('titulo')
                                 .con_genero('genero')
                                 .con_actores('actores')
                                 .con_anio_estreno(2021)
                                 .con_audiencia('audiencia')
                                 .con_pais_origen('pais')
                                 .build

    datos_serie = {
      'id' => 1,
      'titulo' => 'titulo',
      'actores' => 'actores',
      'anio_de_estreno' => 2021,
      'audiencia' => 'audiencia',
      'genero' => 'genero',
      'pais_de_origen' => 'pais',
      'fecha_de_alta' => '2020-03-13',
      'temporadas' => []
    }

    serie_creada = SerieParser.crear(datos_serie)

    expect(serie_creada.id).to eq(serie_esperada.id)
    expect(serie_creada.titulo).to eq(serie_esperada.titulo)
    expect(serie_creada.actores).to eq(serie_esperada.actores)
    expect(serie_creada.anio_de_estreno).to eq(serie_esperada.anio_de_estreno)
    expect(serie_creada.audiencia).to eq(serie_esperada.audiencia)
    expect(serie_creada.genero).to eq(serie_esperada.genero)
    expect(serie_creada.pais_de_origen).to eq(serie_esperada.pais_de_origen)
  end
end
