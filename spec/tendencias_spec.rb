require 'spec_helper'
require_relative '../app/api/pelicula'

require "#{File.dirname(__FILE__)}/../app/bot_client"

describe 'Tendencias' do
  let(:token) { 'fake_token' }
  let(:api_mock) { instance_double('Api') }
  let(:app) do
    BotClientEjemplo.crear
                    .con_token(token)
                    .con_api(api_mock)
                    .build
  end

  it 'devuelve las últimas 3 películas más likeadas' do
    stub_get_updates(token, '/tendencias')
    tendencias = [
      PeliculaEjemplo.crear
                     .con_id(2)
                     .con_titulo('Esperando a la Carroza')
                     .con_director('Alejandro Doria')
                     .build,
      PeliculaEjemplo.crear
                     .con_id(3)
                     .con_titulo('James Bond')
                     .con_director('Sam Mendes')
                     .build,
      PeliculaEjemplo.crear
                     .con_id(4)
                     .con_titulo('Harry Potter')
                     .con_director('David Yates')
                     .build
    ]

    stub_send_message(token, <<~TENDENCIAS.chomp
      Peli: Esperando a la Carroza (2)
      Peli: James Bond (3)
      Peli: Harry Potter (4)
    TENDENCIAS
    )

    allow(api_mock).to receive(:tendencias).and_return(tendencias)
    app.run_once
    expect(api_mock).to receive(:tendencias)
  end

  it 'devuelve "No hay tendencias" si no hay tendencias' do
    stub_get_updates(token, '/tendencias')
    stub_send_message(token, 'No hay tendencias')

    allow(api_mock).to receive(:tendencias).and_return([])
    app.run_once
    expect(api_mock).to receive(:tendencias)
  end

  it 'devuelve "Error inesperado. Intente nuevamente en unos minutos" si ocurre una excepción inesperada' do
    stub_get_updates(token, '/tendencias')
    stub_send_message(token, 'Error inesperado. Intente nuevamente en unos minutos')

    allow(api_mock).to receive(:tendencias).and_raise(ConexionError.new)

    app.run_once
  end
end
