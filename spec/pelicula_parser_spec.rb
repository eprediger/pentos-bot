require 'spec_helper'
require_relative '../app/parsers/pelicula_parser'
require_relative './ejemplos/pelicula_ejemplo'

describe 'PeliculaParser' do
  it 'debe crear la pelicula con sus atributos' do
    pelicula_esperada = PeliculaEjemplo.crear
                                       .con_id(1)
                                       .con_titulo('titulo')
                                       .con_actores('actores')
                                       .con_anio_estreno(2021)
                                       .con_audiencia('audiencia')
                                       .con_director('director')
                                       .con_duracion(100)
                                       .con_genero('genero')
                                       .con_pais_origen('pais')
                                       .build

    datos_pelicula = {
      'id' => 1,
      'titulo' => 'titulo',
      'actores' => 'actores',
      'anio_de_estreno' => 2021,
      'audiencia' => 'audiencia',
      'director' => 'director',
      'duracion' => 100,
      'genero' => 'genero',
      'pais_de_origen' => 'pais',
      'fecha_de_alta' => '2020-03-13'
    }

    pelicula_creada = PeliculaParser.crear(datos_pelicula)

    expect(pelicula_creada.id).to eq(pelicula_esperada.id)
    expect(pelicula_creada.titulo).to eq(pelicula_esperada.titulo)
    expect(pelicula_creada.actores).to eq(pelicula_esperada.actores)
    expect(pelicula_creada.anio_de_estreno).to eq(pelicula_esperada.anio_de_estreno)
    expect(pelicula_creada.audiencia).to eq(pelicula_esperada.audiencia)
    expect(pelicula_creada.director).to eq(pelicula_esperada.director)
    expect(pelicula_creada.duracion).to eq(pelicula_esperada.duracion)
    expect(pelicula_creada.genero).to eq(pelicula_esperada.genero)
    expect(pelicula_creada.pais_de_origen).to eq(pelicula_esperada.pais_de_origen)
  end
end
