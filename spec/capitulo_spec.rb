require 'spec_helper'

describe 'Capitulo' do
  it 'debe devolver el detalle' do
    capitulo = CapituloEjemplo.crear
                              .con_numero(1)
                              .con_titulo('1:23:45')
                              .con_director('Johan Renck')
                              .con_duracion(58)
                              .build

    expect(capitulo.obtener_detalle).to eq('1: "1:23:45" (58 minutos). Director: Johan Renck')
  end
end
