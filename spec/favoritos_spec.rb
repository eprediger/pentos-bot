require 'spec_helper'
require_relative 'bot_client_ejemplo'
require_relative '../app/errors/request_invalido_error'
require_relative '../app/errors/conexion_error'

describe 'Lista de Favoritos' do
  let(:token) { 'fake_token' }
  let(:api_mock) { instance_double('Api') }
  let(:app) do
    BotClientEjemplo.crear
                    .con_token(token)
                    .con_api(api_mock)
                    .build
  end

  context 'cuando se quiere agregar contenido a la lista' do # rubocop:disable RSpec/ContextWording
    it 'devuelve "Se agregó a tu lista correctamente" si se indica un contenido valido' do
      stub_get_updates(token, '/guardar 1', 'egutter')
      stub_send_message(token, 'Se agregó a tu lista correctamente')
      allow(api_mock).to receive(:agregar_a_lista)

      app.run_once

      expect(api_mock).to have_received(:agregar_a_lista).with('egutter', 1)
    end

    it 'devuelve "No se indicó el contenido a guardar." cuando no se indica un id' do
      stub_get_updates(token, '/guardar')
      stub_send_message(token, 'No se indicó el contenido a guardar.')

      expect(api_mock).not_to receive(:agregar_a_lista)

      app.run_once
    end

    it 'devuelve "Contenido invalido. El id debe ser un número" cuando se envía /guardar con un id que no es numérico' do
      stub_get_updates(token, '/guardar una letra')
      stub_send_message(token, 'Contenido invalido. El id debe ser un número')

      expect(api_mock).not_to receive(:agregar_a_lista)

      app.run_once
    end

    it 'devuelve "Contenido no existente" si no existe el contenido' do
      stub_get_updates(token, '/guardar 5')
      stub_send_message(token, 'Contenido no existente')

      allow(api_mock).to receive(:agregar_a_lista).and_raise(RequestInvalidoError.new('Contenido no existente'))

      app.run_once
    end
  end

  context 'cuando se quiere consultar contenido de la lista de favoritos' do # rubocop:disable RSpec/ContextWording
    it 'devuelve "No tenes favoritos." si no se agregó ninguno previamente' do
      stub_get_updates(token, '/milista', 'egutter')
      stub_send_message(token, 'No tenes favoritos.')
      allow(api_mock).to receive(:consultar_favoritos).and_return([])

      app.run_once

      expect(api_mock).to have_received(:consultar_favoritos).with('egutter')
    end

    it 'devuelve los favoritos guardados' do
      stub_get_updates(token, '/milista', 'egutter')
      favoritos = [
        PeliculaEjemplo.crear
                       .con_id(2)
                       .con_titulo('Esperando a la Carroza')
                       .con_director('Alejandro Doria')
                       .con_genero('Comedia')
                       .build,
        PeliculaEjemplo.crear
                       .con_id(3)
                       .con_titulo('James Bond')
                       .con_director('Sam Mendes')
                       .con_genero('Acción')
                       .build,
        PeliculaEjemplo.crear
                       .con_id(4)
                       .con_titulo('Harry Potter')
                       .con_director('David Yates')
                       .con_genero('Ciencia Ficción')
                       .build
      ]

      stub_send_message(token, <<~PELIS.chomp
        Esperando a la Carroza (2), Comedia
        James Bond (3), Acción
        Harry Potter (4), Ciencia Ficción
      PELIS
      )

      allow(api_mock).to receive(:consultar_favoritos).and_return(favoritos)
      app.run_once

      expect(api_mock).to have_received(:consultar_favoritos).with('egutter')
    end

    it 'devuelve los favoritos guardados con miniseries y peliculas' do
      stub_get_updates(token, '/milista')
      favoritos = [
        PeliculaEjemplo.crear
                       .con_id(2)
                       .con_titulo('Esperando a la Carroza')
                       .con_director('Alejandro Doria')
                       .con_genero('Comedia')
                       .build,
        MiniSerieEjemplo.crear
                        .con_id(3)
                        .con_titulo('Chernobyl')
                        .con_genero('Drama Histórico')
                        .build
      ]

      stub_send_message(token, <<~PELIS.chomp
        Esperando a la Carroza (2), Comedia
        Chernobyl (3), Drama Histórico
      PELIS
      )

      allow(api_mock).to receive(:consultar_favoritos).and_return(favoritos)
      app.run_once

      expect(api_mock).to have_received(:consultar_favoritos).with('egutter')
    end

    it 'devuelve los favoritos guardados con miniseries, peliculas y series' do
      stub_get_updates(token, '/milista')
      favoritos = [
        PeliculaEjemplo.crear
                       .con_id(2)
                       .con_titulo('Esperando a la Carroza')
                       .con_director('Alejandro Doria')
                       .con_genero('Comedia')
                       .build,
        MiniSerieEjemplo.crear
                        .con_id(3)
                        .con_titulo('Chernobyl')
                        .con_genero('Drama Histórico')
                        .build,
        SerieEjemplo.crear
                    .con_id(4)
                    .con_genero('Suspenso')
                    .con_titulo('La casa de papel')
                    .build
      ]

      stub_send_message(token, <<~PELIS.chomp
        Esperando a la Carroza (2), Comedia
        Chernobyl (3), Drama Histórico
        La casa de papel (4), Suspenso
      PELIS
      )

      allow(api_mock).to receive(:consultar_favoritos).and_return(favoritos)
      app.run_once

      expect(api_mock).to have_received(:consultar_favoritos).with('egutter')
    end
  end
end
