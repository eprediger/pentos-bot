require_relative '../../app/api/temporada'
require_relative 'capitulo_ejemplo'

class TemporadaEjemplo
  def self.crear
    new
  end

  def initialize
    @numero = 1
    @capitulos = [CapituloEjemplo.crear.build]
  end

  def con_numero(numero)
    @numero = numero
    self
  end

  def con_capitulos(capitulos)
    @capitulos = capitulos
    self
  end

  def build
    Temporada.new(numero: @numero, capitulos: @capitulos)
  end
end
