require_relative '../../app/api/capitulo'

class CapituloEjemplo
  def self.crear
    new
  end

  def initialize
    @numero = 1
    @titulo = '1:23:45'
    @director = 'Johan Renck'
    @duracion = 58
  end

  def con_numero(numero)
    @numero = numero
    self
  end

  def con_titulo(titulo)
    @titulo = titulo
    self
  end

  def con_director(director)
    @director = director
    self
  end

  def con_duracion(duracion)
    @duracion = duracion
    self
  end

  def build
    Capitulo.new(
      numero: @numero,
      titulo: @titulo,
      director: @director,
      duracion: @duracion
    )
  end
end
