require_relative '../../app/api/serie'
require_relative 'temporada_ejemplo'

class SerieEjemplo
  def self.crear
    new
  end

  def initialize
    @id = 4
    @titulo = 'La casa de papel'
    @actores = 'Úrsula Corberó, Álvaro Morte, Itzar Ituño'
    @anio_estreno = 2017
    @audiencia = '16+'
    @genero = 'Suspenso'
    @pais_origen = 'España'
    @fecha_alta = Date.parse('2020-03-13')
    @temporadas = [TemporadaEjemplo.crear.build]
  end

  def con_id(id)
    @id = id
    self
  end

  def con_titulo(titulo)
    @titulo = titulo
    self
  end

  def con_actores(actores)
    @actores = actores
    self
  end

  def con_anio_estreno(anio_estreno)
    @anio_estreno = anio_estreno
    self
  end

  def con_audiencia(audiencia)
    @audiencia = audiencia
    self
  end

  def con_genero(genero)
    @genero = genero
    self
  end

  def con_pais_origen(pais)
    @pais_origen = pais
    self
  end

  def con_fecha_de_alta(fecha)
    @fecha_alta = fecha
    self
  end

  def con_temporadas(temporadas)
    @temporadas = temporadas
    self
  end

  def build
    Serie.new(
      id: @id,
      titulo: @titulo,
      anio_de_estreno: @anio_estreno,
      pais_de_origen: @pais_origen,
      genero: @genero,
      audiencia: @audiencia,
      actores: @actores,
      fecha_de_alta: @fecha_alta,
      temporadas: @temporadas
    )
  end
end
