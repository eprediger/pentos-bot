require_relative './capitulo_ejemplo'
require_relative '../../app/api/mini_serie'

class MiniSerieEjemplo
  def self.crear
    new
  end

  def initialize
    @id = 1
    @titulo = 'Chernobyl'
    @anio_estreno = 2019
    @pais_origen = 'Estados Unidos'
    @genero = 'Drama'
    @audiencia = '16+'
    @actores = 'Jared Harris, Stellan Skarsgård, Emily Watson'
    @fecha_alta = Date.parse('2020-03-13')
    @capitulos = [CapituloEjemplo.crear.build]
  end

  def con_id(id)
    @id = id
    self
  end

  def con_titulo(titulo)
    @titulo = titulo
    self
  end

  def con_anio_estreno(anio_estreno)
    @anio_estreno = anio_estreno
    self
  end

  def con_pais_origen(pais)
    @pais_origen = pais
    self
  end

  def con_genero(genero)
    @genero = genero
    self
  end

  def con_audiencia(audiencia)
    @audiencia = audiencia
    self
  end

  def con_actores(actores)
    @actores = actores
    self
  end

  def con_fecha_de_alta(fecha)
    @fecha_alta = fecha
    self
  end

  def con_capitulos(capitulos)
    @capitulos = capitulos
    self
  end

  def build
    MiniSerie.new(
      id: @id,
      titulo: @titulo,
      anio_de_estreno: @anio_estreno,
      pais_de_origen: @pais_origen,
      genero: @genero,
      audiencia: @audiencia,
      actores: @actores,
      fecha_de_alta: @fecha_alta,
      capitulos: @capitulos
    )
  end
end
