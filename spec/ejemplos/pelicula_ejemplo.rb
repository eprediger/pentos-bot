class PeliculaEjemplo
  def self.crear
    new
  end

  def initialize
    @id = 1
    @titulo = 'Star Wars'
    @director = 'George Lucas'
    @anio_estreno = 1977
    @pais_origen = 'Estados Unidos'
    @genero = 'Ciencia Ficción'
    @audiencia = '10+'
    @actores = 'Mark Hamill, Harrison Ford, Carrie Fisher'
    @duracion = 121
    @fecha_alta = Date.parse('2020-10-01')
  end

  def con_id(id)
    @id = id
    self
  end

  def con_titulo(titulo)
    @titulo = titulo
    self
  end

  def con_director(director)
    @director = director
    self
  end

  def con_anio_estreno(anio_estreno)
    @anio_estreno = anio_estreno
    self
  end

  def con_pais_origen(pais)
    @pais_origen = pais
    self
  end

  def con_genero(genero)
    @genero = genero
    self
  end

  def con_audiencia(audiencia)
    @audiencia = audiencia
    self
  end

  def con_actores(actores)
    @actores = actores
    self
  end

  def con_duracion(duracion)
    @duracion = duracion
    self
  end

  def con_fecha_de_alta(fecha)
    @fecha_alta = fecha
    self
  end

  def build
    Pelicula.new(
      id: @id,
      titulo: @titulo,
      director: @director,
      anio_de_estreno: @anio_estreno,
      pais_de_origen: @pais_origen,
      genero: @genero,
      audiencia: @audiencia,
      actores: @actores,
      duracion: @duracion,
      fecha_de_alta: @fecha_alta
    )
  end
end
