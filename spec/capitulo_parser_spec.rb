require 'spec_helper'
require_relative '../app/parsers/capitulo_parser'
require_relative './ejemplos/capitulo_ejemplo'

describe 'CapituloParser' do
  it 'debe crear el capítulo con sus atributos' do
    capitulo_esperado = CapituloEjemplo.crear
                                       .con_numero(1)
                                       .con_titulo('titulo')
                                       .con_director('director')
                                       .con_duracion(45)
                                       .build

    datos_capitulo = {
      'numero' => 1,
      'titulo' => 'titulo',
      'director' => 'director',
      'duracion' => 45
    }

    capitulo_creado = CapituloParser.crear(datos_capitulo)

    expect(capitulo_creado.numero).to eq(capitulo_esperado.numero)
    expect(capitulo_creado.titulo).to eq(capitulo_esperado.titulo)
    expect(capitulo_creado.director).to eq(capitulo_esperado.director)
    expect(capitulo_creado.duracion).to eq(capitulo_esperado.duracion)
  end
end
