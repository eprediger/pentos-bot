class BotClientEjemplo
  def self.crear
    new
  end

  def initialize
    require 'rspec/mocks'
    extend RSpec::Mocks::ExampleMethods
    RSpec::Mocks.setup
    @token = 'fake_token'
    @logger_level = 0
    @api = instance_double('Api')
  end

  def con_token(token)
    @token = token
    self
  end

  def con_logger_level(logger_level)
    @logger_level = logger_level
    self
  end

  def con_api(api)
    @api = api
    self
  end

  def build
    BotClient.new(@token, @api, @logger_level)
  end
end
