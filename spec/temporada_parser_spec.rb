require 'spec_helper'
require_relative '../app/parsers/temporada_parser'
require_relative './ejemplos/temporada_ejemplo'

describe 'TemporadaParser' do
  it 'debe crear la temporada con sus atributos' do
    temporada_esperada = TemporadaEjemplo.crear
                                         .con_numero(1)
                                         .con_capitulos([])
                                         .build

    datos_temporada = {
      'numero' => 1,
      'capitulos' => []
    }

    temporada_creada = TemporadaParser.crear(datos_temporada)

    expect(temporada_creada.numero).to eq(temporada_esperada.numero)
  end
end
