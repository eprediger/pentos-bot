require 'spec_helper'

describe 'Consulta por Género' do
  let(:token) { 'fake_token' }
  let(:api_mock) { instance_double('Api') }
  let(:app) do
    BotClientEjemplo.crear
                    .con_token(token)
                    .con_api(api_mock)
                    .build
  end

  it 'debe devolver los contenidos del Género indicado' do
    stub_get_updates(token, '/genero Acción')

    contenidos = [
      PeliculaEjemplo.crear.con_id(3).con_titulo('James Bond').con_genero('Acción').build,
      SerieEjemplo.crear.con_id(5).con_titulo('Arrow').con_genero('Acción').build,
      PeliculaEjemplo.crear.con_id(6).con_titulo('Terminator 2').con_genero('Acción').build
    ]

    stub_send_message(token, <<~BUSQUEDA.chomp
      Peli: James Bond (3)
      Serie: Arrow (5)
      Peli: Terminator 2 (6)
    BUSQUEDA
    )

    allow(api_mock).to receive(:contenido_por_genero).with('Acción').and_return(contenidos)
    app.run_once
  end

  it 'debe informar cuando no se encontraron contenidos para el Genero indicado' do
    stub_get_updates(token, '/genero drama')

    contenidos = []

    stub_send_message(token, 'No hay contenido del género')

    allow(api_mock).to receive(:contenido_por_genero).with('drama').and_return(contenidos)
    app.run_once
  end

  it 'debe informar cuando la consulta corresponde a un género inexistente' do
    stub_get_updates(token, '/genero inexistente')

    stub_send_message(token, 'El Género indicado no existe')

    allow(api_mock).to receive(:contenido_por_genero).with('inexistente').and_raise(RequestInvalidoError.new('El Género indicado no existe'))
    app.run_once
  end

  it 'debe informar cuando no se indicó el nombre del género' do
    stub_get_updates(token, '/genero')

    mensaje = 'No se indico de que género se quiere contenido.'
    stub_send_message(token, mensaje)

    expect(api_mock).not_to receive(:detalle_contenido)
    app.run_once
  end
end
